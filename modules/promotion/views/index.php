<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	
	// if (getCountryUser() == 'id')
	// {
		// section_content('
		// <div class="h1 d-none d-md-block">Nikmati promo dari kami</div>
		// <div class="h3 d-block d-md-none">Nikmati promo dari kami</div>');
	/*
	}
	elseif (getCountryUser() == 'en') 
	{
		section_content('
		<div class="h1 d-none d-md-block">Enjoy attractive promos from us</div>
		<div class="h3 d-block d-md-none">Enjoy attractive promos from us</div>');
	}
	*/
	// section_content('
	// 	</div>

	// 	<div class="position-relative mb-5">
	// 		<div class="row no-gutters">'
	// );
	section_content('
		<div class="container py-5">
			<div class="subheading-ob-r mb-3">Promo</div>
			<div class="h1 d-none d-md-block">Nikmati promo dari kami</div>
			<div class="h3 d-block d-md-none">Nikmati promo dari kami</div>
		</div>
	');
	// If data promo is null
	if($data == NULL){
		section_content('<h3 class="text-center" style="padding: 5rem 0 10rem 0;">Nantikan promo kami selanjutnya</h3>');
	} else {
		section_content('
			<div class="position-relative mb-5">
				<div class="row no-gutters">
		');
		foreach ($data as $key => $value)
		{
			if ($value['promo_featured'] == 1)
			{
				section_content('
					<div class="col-12 mb-5">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#promo'.$value['promo_id'].'"><img src="'.$value['promo_image'].'" class="w-100 img-fluid"></a>
						
						<!-- Modal -->
						<div class="modal fade" id="promo'.$value['promo_id'].'" tabindex="-1" aria-labelledby="examplePromo'.$value['promo_id'].'" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="max-width: 600px">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="examplePromo'.$value['promo_id'].'">'.$value['promo_name'].'</h5>
										
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
						
									<div class="modal-body">
										'.nl2br($value['promo_terms_condition']).'

										<div class="mt-3">
											<div class="mb-2">Promo Start: '.get_date(strtotime($value['promo_start_time']), 'date').'</div>
											<div>Promo End: '.get_date(strtotime($value['promo_end_time']), 'date').'</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>');
			}
		}

		section_content('
				<!---
				<div class="col-12 mb-5">
					<img src="'.base_url('assets/image_promo/image_16(2).jpg').'" class="w-100 img-fluid">
				</div>

				<div class="col-12 mb-5">
					<img src="'.base_url('assets/image_promo/image_16(3).jpg').'" class="w-100 img-fluid">
				</div>

				<div class="col-12 mb-5">
					<img src="'.base_url('assets/image_promo/image_16(4).jpg').'" class="w-100 img-fluid">
				</div>

				<div class="col-12 mb-5">
					<img src="'.base_url('assets/image_promo/image_16(5).jpg').'" class="w-100 img-fluid">
				</div>

				<div class="col-12 mb-5">
					<img src="'.base_url('assets/image_promo/image_16(6).jpg').'" class="w-100 img-fluid">
				</div>
				--->
			</div>

			<div class="text-center">
				<a href="https://order.btdelivery.com" target="_blank" class="btn btn-bt-orange px-3 py-2"><img src="'.base_url('assets/images/icon-cart-white.svg').'" class="img-fluid align-middle mr-1"> Get Orders</a>
			</div>
		</div>');
	}
?>
