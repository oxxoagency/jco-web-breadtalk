<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

defined('MODULEPATH') OR exit('No direct script access allowed');

class promotion 
{
	protected $ext;
	
	public function __construct() 
	{
		$this->ext = load_ext(['url']);
	}

	public function index()
	{
		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$params = json_encode([
			'brand' 			=> 3,
			// 'locale' 			=> ''
			'locale' 			=> getCountryUser()
		]);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/promo');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$http 	= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);
		
		if ($http != 200)
		{
			redirect('home/error404');
		}

		$data['data']	= $getRes['data'];

		return view('index', $data);
	}
}

?>