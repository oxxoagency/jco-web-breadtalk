<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('
	<style>
	.bt-nav-category div a
	{
		color: #C4C4C4;
		text-decoration: none;
	}

	.bt-nav-category div a > img
	{
		width: 32px;
		height: 32px;
	}

	.bt-nav-category div a:hover,
	.bt-nav-category div a.active
	{
		color: #fff;
		font-weight: 600;
	}

	.bt-card .bt-short-desc 
	{
		display: none;
		content: "";
		top: 0;
		left: 0;
		bottom: 45px;
		right: 0;
		position: absolute;
		color: #ffffff;
		max-width: 100%;
		max-height: 100%;
		background: rgba(0, 0, 0, 0.64);
	}
	</style>

	<div class="bt-coverimage-w-overlay">
		<img src="'.base_url('assets/image_products/bread/IMG-20180508-WA0060_1.jpg').'" class="w-100 img-fluid d-none d-lg-block">
		<img src="'.base_url('assets/image_product_mobile/bread/IMG-20180508-WA0060-1-mobile.jpg').'" class="w-100 img-fluid d-block d-lg-none">
	
		<div class="position-absolute text-center text-white d-flex align-items-center justify-content-center px-3" style="top: 0;left: 0;bottom: 0;right: 0;width: 100%;height: 100%;z-index: 1">
			<div class="row">
				<div class="col-md-8 mx-auto">
					<div class="subheading-ob-lr-white mb-3">Our Product</div>

					<div class="h1 d-none d-lg-block">Celebrating the best in Indonesia contemporary Bread all around Indonesia</div>
					<div class="h3 d-block d-lg-none">Celebrating the best in Indonesia contemporary Bread all around Indonesia</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-bt-black-linear bt-nav-category-container sticky-top py-0 py-md-2">
		<div class="row no-gutters bt-nav-category justify-content-lg-center text-nowrap flex-nowrap flex-xl-wrap">
			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!" class="active">
					<img src="'.base_url('assets/icons/icon-category-bread-white.svg').'">

					<div class="mt-2">
						Bread
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-cookie-white.svg').'">

					<div class="mt-2">
						Cookies
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-danish-white.svg').'">

					<div class="mt-2">
						Danish
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-drycakes-white.svg').'">

					<div class="mt-2">
						Dry Cakes
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-hampers-white.svg').'">

					<div class="mt-2">
						Hampers
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-slicecakes-white.svg').'">

					<div class="mt-2">
						Slice Cakes
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-toast-white.svg').'">

					<div class="mt-2">
						Toast
					</div>
				</a>
			</div>

			<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 text-center">
				<a href="#!">
					<img src="'.base_url('assets/icons/icon-category-wholecakes-white.svg').'">

					<div class="mt-2">
						Whole Cakes
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="container my-5">
		<h4 class="mb-4">Bread</h4>

		<div class="row bt-list-product mb-5">
			<!--- List Product 1 --->
			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Almond_Blanc_2.jpg').'" class="d-block mx-auto">

						<div class="bt-short-desc m-2 p-2">
							<div class="d-flex justify-content-center align-items-center text-center w-100 h-100">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>

					<a href="#!" class="text-decoration-none stretched-link d-none d-md-block">
						<h6 class="mb-2 text-dark text-truncate">Almond Blanch</h6>
					</a>

					<a href="#collapseExample1" class="text-decoration-none bt-collapse d-block d-md-none" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample1">
						<h6 class="mb-2 text-dark text-truncate d-inline-block">Almond Blanch</h6>
					</a>

					<div class="collapse text-bt-grey" id="collapseExample1">
						Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
					</div>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Banana_Lekker_1.jpg').'" class="d-block mx-auto">
					
						<div class="bt-short-desc m-2 p-2">
							<div class="d-flex justify-content-center align-items-center text-center w-100 h-100">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>

					<a href="#" class="text-decoration-none stretched-link d-none d-md-block">
						<h6 class="mb-2 text-dark text-truncate">Banana Lekker</h6>
					</a>

					<a href="#collapseExample2" class="text-decoration-none bt-collapse d-block d-md-none" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample1">
						<h6 class="mb-2 text-dark text-truncate d-inline-block">Banana Lekker</h6>
					</a>

					<div class="collapse text-bt-grey" id="collapseExample2">
						Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
					</div>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/BANANA_TIGERS_1.jpg').'" class="d-block mx-auto">
					
						<div class="bt-short-desc m-2 p-2">
							<div class="d-flex justify-content-center align-items-center text-center w-100 h-100">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Banana Tigers</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/BANK_OF_CHOCOLATE_1.jpg').'" class="d-block mx-auto">
					
						<div class="bt-short-desc m-2 p-2">
							<div class="d-flex justify-content-center align-items-center text-center w-100 h-100">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Bank of chocolate</h6>
					</a>
				</div>
			</div>

			<!--- List Product 2 --->
			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Beef_Milano_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Beef Milano</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/BinoQ_Fire_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">BinoQ Fire</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Blueberry_Rocky_Milk_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Blueberry Rocky Milk</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/C\'s_Flosss_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">C\'s Flosss</h6>
					</a>
				</div>
			</div>

			<!--- List Product 3 --->
			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Caffe_Uno_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Caffe Uno</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Cheese_Boat_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Cheese Boat</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Chibi_Bun_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Chibi Bun</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Choco_Lava_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Choco Lava</h6>
					</a>
				</div>
			</div>

			<!--- List Product 4 --->
			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Choco_Meyses_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Choco Meyses</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Choconut_Cheese_Roll_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Choconut Cheese Roll</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/Ciabatta_Pizza_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">Ciabatta Pizza</h6>
					</a>
				</div>
			</div>

			<div class="col-md-3 col-lg-3 mb-4">
				<div class="bt-card rounded p-2">
					<div class="img-container d-flex align-items-center w-100 rounded mb-3">
						<img src="'.base_url('assets/image_products/bread/D\'Diva_1.jpg').'" class="d-block mx-auto">
					</div>

					<a href="#" class="text-decoration-none stretched-link">
						<h6 class="mb-2 text-dark text-truncate">D\'Diva</h6>
					</a>
				</div>
			</div>
		</div>

		<div class="d-flex justify-content-center">
			<nav aria-label="Page navigation example">
				<ul class="pagination bt-pagination">
					<li class="page-item">
						<a class="page-link" href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fas fa-arrow-left"></i></span>
						</a>
					</li>

					<li class="page-item active"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">4</a></li>
					<li class="page-item"><a class="page-link" href="#">5</a></li>
					<li class="page-item"><a class="page-link" href="#">...</a></li>
					
					<li class="page-item">
						<a class="page-link" href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>');

?>