<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	/*
	 * Update 21052021
	 */

	section_content('
	<style>
	.bt-nav-category div a
	{
		color: #C4C4C4;
		text-decoration: none;
	}

	.bt-nav-category div a > img
	{
		width: 32px;
		height: 32px;
		filter: brightness(0) invert(1);
	}

	.bt-nav-category div a:hover,
	.bt-nav-category div a.active
	{
		color: #fff;
		font-weight: 600;
	}

	.bt-nav-category div a:hover img,
	.bt-nav-category div a.active img
	{
		filter: none;

	}

	.bt-card .bt-short-desc 
	{
		display: none;
		content: "";
		top: 0;
		left: 0;
		bottom: 44px;
		right: 0;
		position: absolute;
		color: #ffffff;
		max-width: 100%;
		max-height: 100%;
		background: rgba(0, 0, 0, 0.64);
	}

	.bt-pagination .page-item:first-child .page-link
	{
		content: "asdasd"
	}

	.bt-collapses-active::after
	{
		-webkit-transform: rotate(180deg);
		transform: rotate(180deg);
	}

	.bt-collapses::after
	{
		display: inline-block;
		font-style: normal;
		font-variant: normal;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		font-family: \'Font Awesome 5 Free\';
		font-weight: 700;
		content: "\f078";
		color: #F68B1E;
		transition: all .15s ease;
		position: absolute;
		right: 10px;
	}
	</style>

	<div class="bt-coverimage-w-overlay d-none">
		<img src="'.base_url('assets/image_products/bread/IMG-20180508-WA0060_1.jpg').'" class="w-100 img-fluid d-none d-lg-block">
		<img src="'.base_url('assets/image_product_mobile/bread/IMG-20180508-WA0060-1-mobile.jpg').'" class="w-100 img-fluid d-block d-lg-none">
	
		<div class="position-absolute text-center text-white d-flex align-items-center justify-content-center px-3" style="top: 0;left: 0;bottom: 0;right: 0;width: 100%;height: 100%;z-index: 1">
			<div class="row">
				<div class="col-md-8 mx-auto">
					<div class="subheading-ob-lr-white mb-3">Our Product</div>

					<div class="h1 d-none d-lg-block">Celebrating the best in Indonesia contemporary Bread all around Indonesia</div>
					<div class="h3 d-block d-lg-none">Celebrating the best in Indonesia contemporary Bread all around Indonesia</div>
				</div>
			</div>
		</div>
	</div>

	<div id="bt-search-product">
		<div class="bg-bt-black-linear bt-nav-category-container sticky-top py-0 py-md-2 bt-data-fetch" data-url="'.site_url('product/getListProductReal').'">
			<div class="position-relative">
				<div class="row no-gutters bt-nav-category justify-content-xl-center text-nowrap flex-nowrap flex-xl-wrap">');

				foreach ($category as $key => $value) 
				{
					$initCategoryName = str_replace(" ", "", $value['category_name']);
					$initCategoryName = str_replace("&", "", $initCategoryName);
					$initCategoryName = str_replace("!", "", $initCategoryName);
					$initCategoryName = str_replace("(", "", $initCategoryName);
					$initCategoryName = str_replace(")", "", $initCategoryName);

					$getCategoryName = str_replace("'", "", $initCategoryName);

					section_content('
					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \''.$getCategoryName.'\'}" @click.prevent="selectCategory(\''.$value['category_id'].'\', \''.$value['category_name'].'\')">
							<img src="'.$value['category_icon'].'">

							<div class="mt-2">
								'.$value['category_name'].'
							</div>
						</a>
					</div>');
				}

	section_content('
					<!---
					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'cookies\'}" @click.prevent="selectCategory(\'cookies\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-cookie-orange.svg').'">

							<div class="mt-2">
								Cookies
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'danish\'}" @click.prevent="selectCategory(\'danish\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-danish-orange.svg').'">

							<div class="mt-2">
								Danish
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'dry_cake\'}" @click.prevent="selectCategory(\'dry_cake\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-drycakes-orange.svg').'">

							<div class="mt-2">
								Dry Cake
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'hampers\'}" @click.prevent="selectCategory(\'hampers\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-hampers-orange.svg').'">

							<div class="mt-2">
								Hampers
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'slice_cake\'}" @click.prevent="selectCategory(\'slice_cake\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-slicecakes-orange.svg').'">

							<div class="mt-2">
								Slice Cake
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'toast\'}" @click.prevent="selectCategory(\'toast\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-toast-orange.svg').'">

							<div class="mt-2">
								Toast
							</div>
						</a>
					</div>

					<div class="col-3 col-lg-2 col-xl-1 py-3 px-xl-3 mx-xl-2 bt-nav-list text-center">
						<a href="javascript:void(0);" v-bind:class="{ active: selectedCat == \'whole_cake\'}" @click.prevent="selectCategory(\'whole_cake\')">
							<img src="'.base_url('assets/icons/category_orange/icon-category-wholecakes-orange.svg').'">

							<div class="mt-2">
								Whole Cake
							</div>
						</a>
					</div>
					--->
				</div>

				<div class="text-white d-block d-xl-none">
					<a href="javascript:void(0)" class="bt-leftArrowNav position-absolute d-none" style="left: 0;top: 20px;bottom: 20px;width: 2rem;font-size: 18px;background: rgba(0, 0, 0, .25);">
						<div class="d-flex align-items-center justify-content-center h-100">
							<i class="fas fa-chevron-left text-white"></i>
						</div>
					</a>

					<a href="javascript:void(0)" class="bt-rightArrowNav position-absolute" style="right: 0;top: 20px;bottom: 20px;width: 2rem;font-size: 18px;background: rgba(0, 0, 0, .25);">
						<div class="d-flex align-items-center justify-content-center h-100">	
							<i class="fas fa-chevron-right text-white"></i>
						</div>
					</a>
				</div>
			</div>
		</div>

		<div class="container bt-container-mobile my-5">
			<div id="bt-item"></div>
			
			<div v-if="loading" class="text-center my-5 py-5 vh-100">
				<div class="bt-loader mx-auto">
					<div class="bt-loading"></div>
					<div class="text"><img src="'.base_url('assets/icons/icon-logo-loading.svg').'"></div>
				</div>
			</div>

			<div v-else-if="statusData == \'fail\'" class="ar-data-status" style="display: none">
				<div class="d-flex justify-content-center align-items-center h5 m-0 py-5 text-danger" style="height: 50vh">
					{{ msgData }}
				</div>
			</div>

			<div v-else class="ar-data-load" style="display: none">
				<div v-if="loadingnextpage" class="text-center py-5">
					<div class="bt-loader mx-auto">
						<div class="bt-loading"></div>
						<div class="text"><img src="'.base_url('assets/icons/icon-logo-loading.svg').'"></div>
					</div>
				</div>	

				<div v-else>
					<div class="row mx-1 mx-md-0">
						<div class="col-6">
							<h4 class="mb-4 text-capitalize">{{ getTitleCategory }}</h4>
						</div>

						<div class="col-6 text-right">
							<a href="javascript:void(0)" class="text-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort By</a>

							<div class="dropdown-menu dropdown-menu-right font-weight-inherit">
								<a href="javascript:void(0);" class="dropdown-item" @click.prevent="sortBy(\'asc\')">Ascending</a>
								<a href="javascript:void(0);" class="dropdown-item" @click.prevent="sortBy(\'desc\')">Descending</a>
							</div>
						</div>
					</div>

					<transition-group name="custom-classes-transition" enter-active-class="animate__animated animate__fadeIn animate__faster" class="row no-gutters mb-5" tag="div">
						<div class="col-md-3 col-lg-3 mb-4" v-for="(info, index) in getData" :key="info.menu_id">
							<div class="bt-card rounded p-0 p-md-2">
								<div class="img-container d-flex align-items-center w-100 rounded mb-3">
									<img v-lazy="\'\'+info.menu_image+\'\'" class="d-block mx-auto lazy" alt="Image">
								</div>

								<div class="px-3 px-md-0">
									<a href="javascript:void(0)" class="text-decoration-none stretched-link d-none d-md-block">
										<h6 class="mb-2 text-dark text-truncate">{{ info.menu_name }}</h6>
									</a>

									<div class="text-bt-grey mt-2 d-none d-md-block">
										{{ info.menu_desc }}
									</div>

									<a :href="\'#collapse\'+info.no+info.menu_codename" v-on:click="clickCollapseToDown($event)" class="bt-collapse-link text-decoration-none d-block d-md-none stretched-link" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample1">
										<h5 class="bt-collapses mb-2 text-dark text-truncate d-inline-block">{{ info.menu_name }}</h5>
									</a>

									<div class="collapse text-bt-grey" :id="\'collapse\'+info.no+info.menu_codename">
										{{ info.menu_desc }}
									</div>
								</div>
							</div>
						</div>
					</transition-group>
				</div>

				<div class="w-100">
					<paginate :page-count="pageCount" :page-range="pageRange" :margin-pages="0" :container-class="\'pagination bt-pagination flex-wrap justify-content-center mt-5\'" :page-class="\'page-item\'" :prev-class="\'page-item\'" :next-class="\'page-item\'" :page-link-class="\'page-link\'" :prev-link-class="\'page-link\'" :next-link-class="\'page-link\'" :click-handler="clickPaginate" v-model="currPage">
						<span slot="prevContent"><i class="fas fa-arrow-left"></i></span>
						<span slot="nextContent"><i class="fas fa-arrow-right"></i></span>
					</paginate>
				</div>
			</div>
		</div>
	</div>');

?>