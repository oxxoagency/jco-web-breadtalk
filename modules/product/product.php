<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

defined('MODULEPATH') OR exit('No direct script access allowed');

class product 
{
	protected $ext;

	protected $output;

	protected $input;

	protected $user_agent;

	protected $num_per_page;
	
	public function __construct() 
	{
		$this->ext = load_ext(['url']);

		$this->output = load_lib('output');

		$this->input = load_lib('input');

		$this->user_agent = load_lib('user_agent');

		$this->num_per_page = num_per_page();
	}

	public function index()
	{
		/*
		 * Update file bt-vue.js to version 5
		 * Update file list BreadTalk Product JSON
		 * Change URL file JSON
		 * Update fix bug
		 */
		
		register_vuejs([
			'<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>',
			'<script src="https://unpkg.com/vuejs-paginate@1.9.4"></script>',
			'<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>',
			'<script src="https://unpkg.com/vue-lazyload@1.3.3/vue-lazyload.js"></script>',
			'<script src="'.base_url('assets/js/bt-vue.js?v=0.0.11').'"></script>'
		]);

		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/menu/category/3');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$http 	= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);

		if ($http != 200)
		{
			redirect('home/error404');
		}

		$data['input'] = $this->input;
		$data['category'] = $getRes['data'];

		return view('index', $data);
	}

	public function getListProductReal()
	{
		$getPage = ! empty($this->input->get('page')) ? $this->input->get('page') : 1;
		$getSort = ! empty($this->input->get('sort')) ? $this->input->get('sort') : '';
		$getCategory = ! empty($this->input->get('category')) ? $this->input->get('category') : '';

		try
		{
			$http_header = [
				'Content-Type: application/json',
				'Authorization: Bearer '.config_item('Token_API')
			];

			$params = json_encode([
				'brand' 			=> 3,
				// 'locale' 			=> '',
				'locale' 			=> getCountryUser(),
				'menu_category' 	=> $getCategory,
				'page'				=> $getPage,
				'sort'				=> $getSort,
				'limit'				=> $this->num_per_page,

			]);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/menu');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			$result = curl_exec($ch);

			if ($result === FALSE)
			{
				$initGetDataPage = '';
				$totalpage 		 = 0;
				$currentpage 	 = 1;

				$getRess[] = ['status' => 'fail', 'msg' => curl_error($ch).curl_errno($ch)];
			}
			else
			{
				$getHTTPCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

				if ($getHTTPCode == 200)
				{
					$getRes = json_decode($result, TRUE);

					$initGetDataPage = isset($getRes['data']) ? array_values(array_slice($getRes['data'], -1))[0] : '';
					$totalpage 		 = isset($initGetDataPage['getDataPage']['total']) ? ceil($initGetDataPage['getDataPage']['total']/$this->num_per_page) : 0;
					$currentpage 	 = isset($initGetDataPage['getDataPage']['current_page']) ? isset($initGetDataPage['getDataPage']['current_page']) : 1;

					if ($getRes['status'] == '404')
					{
						$getRess[] = ['status' => 'fail', 'msg' => 'No items'];
					}
					else
					{
						foreach ($getRes['data'] as $key => $value) 
						{
							if (isset($value['menu_id']))
							{
								$getRess[] = $value;
							}
						}
					}
				}
				else
				{
					$initGetDataPage = '';
					$totalpage 		 = 0;
					$currentpage 	 = 1;

					$getRess[] = ['status' => 'fail', 'msg' => 'Getting error with HTTP Code '.$getHTTPCode];
				}
			}
		
			curl_close($ch);
		}
		catch (Exception $e) 
		{
			$initGetDataPage = '';
			$totalpage 		 = 0;
			$currentpage 	 = 1;

			$getRess[] = ['status' => 'fail', 'msg' => $e->getCode().$e->getMessage()];	
		}

		$getRess[]['getDataPage'] = ['current_page' => $currentpage, 'total' => $totalpage, 'num_per_page' => $this->num_per_page];

		$this->output
				 ->set_status_header(200)
				 ->set_header('Access-Control-Allow-Origin: *')
				 ->set_content_type('application/json', 'utf-8')
				 ->set_output(json_encode($getRess, JSON_PRETTY_PRINT))
				 ->_display();
		exit;
	}
}

?>