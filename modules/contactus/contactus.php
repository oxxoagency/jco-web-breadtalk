<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

defined('MODULEPATH') OR exit('No direct script access allowed');

	/*
	 * Changed code 22042021.
	 * Disable CURL POST Recaptcha Google for experimental feature. 
	 */

class contactus 
{
	protected $ext;
	
	protected $input;

	protected $output;

	protected $formv;

	public function __construct() 
	{
		$this->ext = load_ext(['url']);

		$this->input = load_lib('input');

		$this->output = load_lib('output');

		$this->formv = load_lib('form_validation');
	}

	public function index()
	{
		register_vuejs([
			'<script src="https://www.google.com/recaptcha/api.js" async defer></script>',
			'<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>',
			'<script src="https://unpkg.com/vuejs-paginate@1.9.4"></script>',
			'<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>',
			'<script src="https://unpkg.com/vue-lazyload@1.3.3/vue-lazyload.js"></script>',
			'<script src="'.base_url('assets/js/bt-vue.js?v=0.0.2').'"></script>'
		]);

		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$params = json_encode([
			'brand' 			=> 3,
			// 'locale' 			=> ''
			'locale' 			=> getCountryUser()
		]);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/page');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);

		if ($this->input->post('step') && $this->input->post('step') == 'post')
		{
			$this->formv->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->formv->set_rules('fullname', 'Fullname', 'required');
			$this->formv->set_rules('subject', 'Subject', 'required');
			$this->formv->set_rules('phone_number', 'Phone Number', 'required|regex_match[/^[&\/0-9]+$/i]');
			$this->formv->set_rules('message', 'Message', 'required');
			$this->formv->set_rules('g-recaptcha-response', 'Recaptcha', 'required');

			if ($this->formv->run() == FALSE)
			{
				echo json_encode(['status' => 'fail', 'msg' => $this->formv->error_array()]);
				exit;
			}
			else
			{
				$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
				$userIP = $this->input->ip_address();

				$siteKey = '6LfBhKYaAAAAACU8F6HFcTIp9OEXEtpoNTJ_R3Bm';
				$secretKey = '6LfBhKYaAAAAACeDbnZByL89AFucM3yQnWLsPjvn';

				$url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$recaptchaResponse.'&remoteip='.$userIP;

				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$output = curl_exec($ch);
				curl_close($ch);

				try 
				{
					$http_header = [
						'Content-Type: application/json',
						'Authorization: Bearer '.config_item('Token_API')
					];

					$data = [					
						'brand'				=> 3,
						'contact_name'		=> $this->input->post('fullname'),
						'contact_email'		=> $this->input->post('email'),
						'contact_subject'	=> $this->input->post('subject'),
						'contact_phone'		=> $this->input->post('phone_number'),
						'contact_message'	=> $this->input->post('message')
					];

					$ch2 = curl_init();
					curl_setopt($ch2, CURLOPT_URL, config_item('IP_API').'contact/store');
					curl_setopt($ch2, CURLOPT_HTTPHEADER, $http_header);
					curl_setopt($ch2, CURLOPT_POST, 1);
					curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($data));
					curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);

					$result = curl_exec($ch2);

					if ($result === false) 
					{
						echo json_encode(['status' => 'fail-submitted', 'msg' => curl_error($ch2).curl_errno($ch2)]);	
						exit;
					}
					else
					{
						$http = curl_getinfo($ch2, CURLINFO_HTTP_CODE);

						if ($http == 200)
						{
							$getRes = json_decode($result, TRUE);

							if ($getRes['status'] == 200)
							{
								echo json_encode(['status' => 'ok', 'msg' => 'Success']);			
								exit;
							}
							else
							{
								echo json_encode(['status' => 'fail-submitted', 'msg' => 'Failed to send message']);					
								exit;
							}
						}
						else
						{
							echo json_encode(['status' => 'fail-submitted', 'msg' => 'Getting error with HTTP Code '.$http]);
							exit;
						}
					}

					curl_close($ch2);
				}
				catch (Exception $e) 
				{
					echo json_encode(['status' => 'fail-submitted', 'msg' => $e->getCode().$e->getMessage()]);	
					exit;
				}
			}
		}

		return view('index');
	}

	public function getUserCountry()
	{
		echo getCountryUser();
		exit;
	}

	public function test()
	{
		try 
		{
			$http_header = [
				'Content-Type: application/json',
				'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiOGY3NTM0OGY5MThhZTkwYmExNWUzNWE4YzI5NmI1NTM4OWNiNWMwNTJhMzQ4YTg4OWZkNjY2NDRhZjg5MjdkZTk2YmQ0OGUyNGQxYmQ5YWIiLCJpYXQiOiIxNjA2OTAwNjc5LjI4Njk2MyIsIm5iZiI6IjE2MDY5MDA2NzkuMjg2OTY4IiwiZXhwIjoiMTYzODQzNjY3OS4yNzM0MDMiLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZPF_z92qky6FDIoPxpbQpmE8hgqDUKE9Hffos31TtlSRY9ggbD7x0Qfs6kSqsbmsRU_GIQR-deM78p8Hyt8aU1iAOJP0xnL7IqVP6P-4DY_Hf6NDBwhc0tb8LCPbaIoTes6LpxKEowqXZdrqhsoFR3AM1c837SMEo-JtuBQ3mq1-8sBcKlUHvrh4I5jhUvl5h9ybTwVYHVclNpy-xGN5y6Vwp6x0fHwsi4gizrVgh9j-m5LhHU2_A4fW6UNZx5iJyaz2BMY3BgpFLK5WB72B94YrKUpooZnb-abHs91KLaNXX3PRPa38wk2CShnD3VlR-YkcmsAuyAekKMhNmmumnod-PkHgrTkzJMXc8UD3Lyor34S1IluY6KkxgFn2QKO9VtFBu6nmQoi2gfxWhrGC6GSn6Cx9wxkvgPMVaRQkl70Pq8k0xUKcWvKrSYePXc3DIP7LTu1Hjk8FjFt9Qd9AM2Y2m7VLlHNaeYDd7X5VDPISXDtf_TwTnSCM1Pql72J1r2-Stq1ziXyLiv2aE9sjhNPpijtuad91AvyAM7V9AIqBJtpekldBvhTLVm3X3Ojp7IEeQAdl5fd3loVzAwJp_esEiIMzgBTEtTFUMgJBVVidWt4jR5z_Fp5E8zCkBwWJmPqbiqVaZj3m2MwTPK7VcpvoT4ZrV-QPHQMGMw7UmWk'
			];

			$data = [					
				'brand'				=> '3',
				'contact_name'		=> 'Andhika Adhitia N',
				'contact_email'		=> 'andhika.adhitia96@gmail.com',
				'contact_subject'	=> '3',
				'contact_phone'		=> '089504518506',
				'contact_message'	=> 'Hello World!'
			];

			$ch2 = curl_init();
			curl_setopt($ch2, CURLOPT_URL, 'https://jcoadminapidev.oxxo.co.id/contact/store');
			curl_setopt($ch2, CURLOPT_HTTPHEADER, $http_header);
			curl_setopt($ch2, CURLOPT_POST, 1);
			curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);

			$result = curl_exec($ch2);

			if ($result === false) 
			{
				echo curl_error($ch2).curl_errno($ch2);	
				exit;
			}
			else
			{
				$getRes = json_decode($result, TRUE);

				print_r($result);
				exit;

				/*
				$http = curl_getinfo($ch2, CURLINFO_HTTP_CODE);

				if ($http == 200)
				{
					$getRes = json_decode($result, TRUE);

					if ($getRes['status'] == 200)
					{
						echo 'Success';			
						exit;
					}
					else
					{
						echo 'Failed to send message';					
						exit;
					}
				}
				else
				{
					echo 'Getting error with HTTP Code '.$http;
					exit;
				}
				*/
			}

			curl_close($ch2);
		}
		catch (Exception $e) 
		{
			echo $e->getCode().$e->getMessage();	
			exit;
		}
	}
}

?>