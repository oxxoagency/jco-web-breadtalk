<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('	
	<div class="container mb-5 py-5">			
		<div class="text-center mb-4 mb-md-5 pb-1 pb-md-0">');

	if (getCountryUser() == 'id')
	{
		section_content('
			<div class="subheading-ob-lr mb-1">Hubungi Kami</div>
			<h2 class="text-dark d-none d-md-block">Team kami akan menghubungi anda</h2>
			<h3 class="text-dark d-block d-md-none">Team kami akan menghubungi anda</h3>');
	}
	elseif (getCountryUser() == 'en') 
	{
		section_content('
			<div class="subheading-ob-lr mb-1">Contact Us</div>
			<h2 class="text-dark d-none d-md-block">How can We Help You?</h2>
			<h3 class="text-dark d-block d-md-none">How can We Help You?</h3>');
	}

	$getCountryUser = (getCountryUser() == 'id') ? 'id' : 'en';

	section_content('
		</div>

		<div class="bg-white border rounded shadow p-4 p-md-5 mx-auto" style="max-width: 600px" id="ar-app-form">
			<div class="sk-notice-toast position-relative" aria-live="polite" aria-atomic="true"></div>

			<form action="'.site_url('contactus/index').'" method="post" @submit="submitWithValidate" ref="formHTML" button-block="false" data-reset="true" captcha-reset="true" class="needs-validation" novalidate>
				<div class="form-group">
					<label>'.get_translate('Name', 'Name', $getCountryUser).'</label>
					<input type="text" name="fullname" placeholder="Enter your name" class="form-control form-control-lg font-size-inherit" v-bind:class="{\'is-valid\': this.initForm.fullname == false, \'is-invalid\': this.initForm.fullname == true}">
				
					<div class="invalid-feedback">{{ responseMessage.fullname }} </div>
				</div>

				<div class="form-group">
					<label>'.get_translate('Email', 'Email', $getCountryUser).'</label>
					<input type="text" name="email" placeholder="Enter your email address" class="form-control form-control-lg font-size-inherit" v-bind:class="{\'is-valid\': this.initForm.email == false, \'is-invalid\': this.initForm.email == true}">
				
					<div class="invalid-feedback">{{ responseMessage.email }} </div>
				</div>

				<div class="form-group">
					<label>'.get_translate('Subject', 'Subjek', $getCountryUser).'</label>

					<select name="subject" class="custom-select form-control-lg font-size-inherit mr-sm-2" v-bind:class="{\'is-valid\': this.initForm.subject == false, \'is-invalid\': this.initForm.subject == true}">
						<option value="">Choose...</option>
						<option value="1">In Store Experience</option>
						<option value="2">Delivery Service</option>
						<option value="3">Nutrional Information</option>
						<option value="4">CSR</option>
						<option value="5">Donation</option>
						<option value="6">Investor Relations</option>
						<option value="7">Job Opportunities</option>
						<option value="8">General Enquires</option>
					</select>
				
					<div class="invalid-feedback">{{ responseMessage.subject }} </div>
				</div>

				<div class="form-group">
					<label>'.get_translate('Phone Number', 'Nomor Telpon', $getCountryUser).'</label>
					<input type="text" name="phone_number" placeholder="Phone Number" class="form-control form-control-lg font-size-inherit" v-bind:class="{\'is-valid\': this.initForm.phone_number == false, \'is-invalid\': this.initForm.phone_number == true}">
				
					<div class="invalid-feedback">{{ responseMessage.phone_number }} </div>
				</div>

				<div class="form-group">
					<label>'.get_translate('Message', 'Pesan', $getCountryUser).'</label>
					<textarea name="message" placeholder="Enter your message" class="form-control form-control-lg font-size-inherit text-totalchars" maxlength="1200" rows="4" v-bind:class="{\'is-valid\': this.initForm.message == false, \'is-invalid\': this.initForm.message == true}"></textarea>
					<div class="text-muted mt-2">Maks. ( <span id="totalChars">0</span> / 1200 ) Characters</div>
				
					<div class="invalid-feedback">{{ responseMessage.message }} </div>
				</div>

				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="6LfBhKYaAAAAACU8F6HFcTIp9OEXEtpoNTJ_R3Bm"></div>

					<div class="invalid-feedback d-block">{{ responseGRecaptcha }}</div>
				</div>

				<div class="form-group pb-2">
					<input type="hidden" name="step" value="post">
					<input type="submit" class="btn btn-bt-orange btn-malika btn-lg font-size-inherit px-5 py-3" value="Send Message">
				</div>

				<div class="text-article text-center border-top pt-4 mt-4">
					<p class="mb-0">
						For more information about BreadTalk, please contact via our whatsapp hotline, which is at 0815-899-8000.
					</p>
				</div>
			</form>
		</div>
	</div>');

?>