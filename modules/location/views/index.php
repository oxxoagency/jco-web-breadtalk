<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('
	<!--- Leaflet CSS --->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.0/dist/leaflet.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-search@2.9.9/dist/leaflet-search.min.css">

	<div class="py-5 mx-auto">
		<div class="container pb-5">
			<div class="text-center mb-5">
				<div class="subheading-ob-lr mb-1">Our Store</div>');

	if (getCountryUser() == 'id')
	{
		section_content('
				<h2 class="text-dark d-none d-md-block">'.$header['header_caption_id'].'</h2>
				<h5 class="text-dark d-block d-md-none">'.$header['header_caption_id'].'</h5>');
	}
	elseif (getCountryUser() == 'en') 
	{
		section_content('
				<h2 class="text-dark d-none d-md-block">'.$header['header_caption'].'</h2>
				<h5 class="text-dark d-block d-md-none">'.$header['header_caption'].'</h5>');
	}

	section_content('
			</div>

			<div class="bg-white p-4 shadow">
				<div id="map" class="w-100" style="height: 500px"></div>
			</div>
		
			<!--- Address 1 --->
			<div class="my-5 pt-5">');

			foreach ($outlets as $key => $value) 
			{
				section_content('
				<h4 class="mb-4">'.$value['province'].'</h4>

				<div class="row mb-5">
					<!--- Address Line 1--->');

				foreach ($value['outlet'] as $keys => $values)
				{
					section_content('
					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address active d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">'.$values['outlet_name'].'</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										'.$values['outlet_address'].'
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : '.$values['outlet_weekend_open'].' - '.$values['outlet_weekend_close'].'</div>
												<div class="d-block">Weekday : '.$values['outlet_weekday_open'].' - '.$values['outlet_weekday_close'].'</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="'.$values['outlet_linkdirection'].$values['outlet_latitude'].','.$values['outlet_longitude'].'" target="_blank" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
								<a href="https://maps.google.com/?q='.$values['outlet_latitude'].','.$values['outlet_longitude'].'" target="_blank" class="text-bt-orange pt-3 mt-3 border-top d-none"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>');
				}
				
				section_content('
					</div>');
			}

	$pagination			= load_lib('pagination', array($total));
	$pagination->paras 	= site_url('location');

	section_content('
			</div>

			<div class="d-flex justify-content-center">
				<nav aria-label="Page navigation example">
					'.$pagination->whole_num_bar('justify-content-center').'

					<ul class="pagination bt-pagination d-none">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true"><i class="fas fa-arrow-left"></i></span>
							</a>
						</li>

						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">5</a></li>
						<li class="page-item"><a class="page-link" href="#">...</a></li>
						
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>');


?>