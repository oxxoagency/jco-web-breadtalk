<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('
	<!--- Leaflet CSS --->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.0/dist/leaflet.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-search@2.9.9/dist/leaflet-search.min.css">

	<div class="py-5 mx-auto">
		<div class="container pb-5">
			<div class="text-center mb-5">
				<div class="subheading-ob-lr mb-1">Our Store</div>
				<h2 class="text-dark d-none d-md-block">Currently BreadTalk has approximately <br/> 140 outlets throughout Indonesia</h2>
				<h5 class="text-dark d-block d-md-none">Currently BreadTalk has approximately <br/> 140 outlets throughout Indonesia</h5>
			</div>

			<div class="bg-white p-4 shadow">
				<div id="map" class="w-100" style="height: 500px"></div>
			</div>
		
			<!--- Address 1 --->
			<div class="my-5 pt-5">
				<h4 class="mb-4">Jabodetabek (Jakarta-Bogor-Depok-Tangerang-Bekasi)</h4>

				<div class="row mb-5">
					<!--- Address Line 1--->

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address active d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk (Istana Plaza)</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Ciwalk</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Cihampelas Walk, Jl. Cihampelas No.160, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat 40131.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Festival Citylink Bandung</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<!--- Address Line 2 --->

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk (Istana Plaza)</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Ciwalk</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Cihampelas Walk, Jl. Cihampelas No.160, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat 40131.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Festival Citylink Bandung</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>
				</div>				

				<h4 class="mb-4">Bandung</h4>

				<div class="row mb-4">
					<!--- Address Line 1--->

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address active d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk (Istana Plaza)</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Ciwalk</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Cihampelas Walk, Jl. Cihampelas No.160, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat 40131.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Festival Citylink Bandung</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<!--- Address Line 2 --->

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk (Istana Plaza)</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Ciwalk</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Cihampelas Walk, Jl. Cihampelas No.160, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat 40131.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-4 mb-4">
						<div class="bt-card-address d-flex align-items-start flex-column rounded p-3 p-lg-3">
							<div class="mb-auto">
								<h5 class="mb-3">BreadTalk Festival Citylink Bandung</h5>
							
								<div class="text-article">
									<p class="font-size-inherit">
										Istana Plaza Ground Floor, Jl. Pasir Kaliki No.121-123, Pamoyanan, Cicendo, Bandung City, West Java 40173.
									</p>
								</div>

								<ul class="list-group list-group-flush">
									<li class="list-group-item px-0 border-0">
										<div class="media">
											<img src="'.base_url('assets/images/icon-time-circle.svg').'" class="img-fluid mr-2">

											<div class="media-body">
												<div class="d-block">Weekend : 10.00 - 21.00 WIB</div>
												<div class="d-block">Weekday : 10.00 - 21.00 WIB</div>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="w-100 mt-2">
								<a href="#!" class="text-bt-orange pt-3 mt-3 border-top d-block"><i class="fas fa-directions fa-lg fa-fw"></i> Get Direction</a>
							</div>
						</div>
					</div>
				</div>				
			</div>

			<div class="d-flex justify-content-center">
				<nav aria-label="Page navigation example">
					<ul class="pagination bt-pagination">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true"><i class="fas fa-arrow-left"></i></span>
							</a>
						</li>

						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">5</a></li>
						<li class="page-item"><a class="page-link" href="#">...</a></li>
						
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>');


?>