<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

defined('MODULEPATH') OR exit('No direct script access allowed');

class location
{
	protected $ext;

	protected $input;

	protected $output;

	protected $num_per_page;
	
	public function __construct() 
	{
		$this->ext = load_ext(['url']);

		$this->input = load_lib('input');

		$this->output = load_lib('output');

		$this->num_per_page = num_per_page();
	}

	public function index()
	{
		register_js([
			'<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>',
			'<script src="https://unpkg.com/leaflet@1.3.0/dist/leaflet.js"></script>',
			'<script src="https://cdn.jsdelivr.net/npm/leaflet-search@2.9.9/dist/leaflet-search.src.min.js"></script>',
			'<script src="'.base_url('assets/js/bt-leaflet.js?v=0.0.1').'"></script>'
		]);

		$getPage = ! empty($this->input->get('page')) ? $this->input->get('page') : 1;

		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$params = json_encode([
			'brand' 			=> 3,
			'locale' 			=> '',
			'city' 				=> '',
			'outlet_name'		=> '',
			'page'				=> $getPage,
			'limit'				=> $this->num_per_page
		]);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/outlet');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$http 	= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);

		if ($http != 200)
		{
			redirect('home/error404');
		}

		$data['input']   = $this->input;
		$data['maps'] 	 = $getRes['maps'];
		$data['header']  = $getRes['header'];
		$data['outlets'] = $getRes['data']['data'];
		$data['total'] 	 = $getRes['data']['total'];

		return view('index', $data);
	}

	public function getDataMap()
	{
		$getPage = ! empty($this->input->get('page')) ? $this->input->get('page') : 1;

		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$params = json_encode([
			'brand' 			=> 3,
			'locale' 			=> '',
			'city' 				=> '',
			'outlet_name'		=> '',
			'page'				=> $getPage,
			'limit'				=> $this->num_per_page
		]);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/outlet');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);

		foreach ($getRes['maps'] as $key => $value) 
		{
			$getData[] = 
			[
				'loc' =>
				[
					$value['outlet_latitude'],
					$value['outlet_longitude']
				],
				'title' => $value['title']
			];
		}

		$this->output
				 ->set_status_header(200)
				 ->set_header('Access-Control-Allow-Origin: *')
				 ->set_content_type('application/json', 'utf-8')
				 ->set_output(json_encode($getData, JSON_PRETTY_PRINT))
				 ->_display();
		exit;
	}
}

?>