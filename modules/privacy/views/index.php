<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('
	<style>
	.bt-parent-container 
	{
		margin-top: 4.3rem;
	}
	</style>
	
	<div class="bt-privacy-policy position-relative">
		<div class="container mb-5 py-5">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb bg-transparent mb-5 p-0">
					<li class="breadcrumb-item font-weight-bold active" aria-current="page"><a href="'.site_url().'">Home</a></li>
					<li class="breadcrumb-item font-weight-bold">Privacy Policy</li>
				</ol>
			</nav>

			<div class="subheading-ob-r mb-1">Privacy Policy</div>
			<h2 class="mb-4">BreadTalk Indonesia</h2>

			<div class="bg-light p-4 p-md-5">
				<div class="text-article">
					<h5>Personal Rights</h5>

					<p>
						BreadTalk Delivery takes your privacy seriously. Please read what is written below to find out more about our privacy policy.
					</p>

					<h5>Personal Information</h5>

					<p>
						This policy covers how BreadTalk Delivery treats personal information collected and received by BreadTalk Delivery, including information related to your past use of BreadTalk Delivery products and services. Personal information is information about you that is personally identifiable such as your name, address, e-mail address or telephone number and which is not available in any other way to the general public.
					</p>

					<h5>Scope</h5>

					<p>
						This policy does not apply to the practices of companies that BreadTalk Delivery does not own or control or to people that BreadTalk Delivery does not employ or manage.
					</p>

					<h5>General</h5>

					<p>
						BreadTalk Delivery collects personal information when you register for BreadTalk Delivery, when you use BreadTalk Delivery products or services, when you visit the BreadTalk Delivery page. BreadTalk Delivery can combine information about you that we have with information we get from business partners or other companies.
					</p>

					<p>
						When you register, we ask for information such as your name, address, e-mail address, date of birth, postal code, industry and what you personally like. When you register for BreadTalk Delivery and log in to our service, you are not anonymous to us.
					</p>

					<p>
						BreadTalk Delivery collects information about your transactions with us, including information about your use of the products and services we offer.
					</p>

					<p>
						BreadTalk Delivery automatically receives and records information from your computer and browser, including your IP address, BreadTalk Delivery cookie information, software and hardware attributes, and the page you request.
					</p>

					<h5>Sharing and disclosure of information</h5>

					<p>
						BreadTalk Delivery does not rent, sell, or share personal information about you to other people or unaffiliated companies except to provide the product or service you have requested if we have your permission, or in the following situations:
					</p>

					<ul>
						<li>
							We provide information to trusted partners who work on behalf of or with BreadTalk Delivery under a confidentiality agreement. These companies may use your personal information to help BreadTalk Delivery communicate with you about offers from BreadTalk Delivery and our marketing partners. However, these companies do not have any independent right to share this information.
						</li>

						<li>
							We respond to subpoenas, court orders, or legal processes, or establish or exercise our legal rights or defend against legal claims.
						</li>					

						<li>
							We believe it is necessary to share information in order to investigate, prevent or take action regarding illegal activities, suspected fraud, situations involving possible threats to anyone\'s physical safety, violations of the terms of use of BreadTalk Delivery or as otherwise required by law.
						</li>

						<li>
							We transfer information about you when BreadTalk Delivery is acquired or merged with another company. In this case, BreadTalk Delivery will notify you before information about you is transferred and becomes subject to a different privacy policy.
						</li>
					</ul>

					<h5>Cookies</h5>

					<p>
						BreadTalk Delivery can set and access BreadTalk Delivery cookies on your computer.
					</p>

					<p>
						BreadTalk Delivery uses web beacons to access BreadTalk Delivery cookies on and off our website network and in connection with BreadTalk Delivery products and services.
					</p>
				</div>
			</div>
		</div>
	</div>');

?>