<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

defined('MODULEPATH') OR exit('No direct script access allowed');

class home 
{
	protected $ext;

	protected $input;
	
	public function __construct() 
	{
		$this->ext = load_ext(['url', 'cookie']);

		$this->input = load_lib('input');
	}

	public function index()
	{
		return view('index');
	}

	public function landing()
	{
		/*
		 * Update 28052021
		 */

		$http_header = [
			'Content-Type: application/json',
			'Authorization: Bearer '.config_item('Token_API')
		];

		$params = json_encode([
			'brand' 			=> 3,
			// 'locale' 			=> ''
			'locale' 			=> getCountryUser()
		]);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, config_item('IP_API').'cms/home/page');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		$http 	= curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$getRes = json_decode($result, TRUE);

		curl_close($ch);
		
		/*
		if ($http != 200)
		{
			redirect('home/error404');
		}
		*/

		$data['banners']	= $getRes['banners'];
		$data['header']		= $getRes['header'];
		$data['categories']	= $getRes['categories'];

		return view('landing', $data);
	}

	public function error404()
	{
		return view('error404');
	}

	public function setlanguage()
	{
		$allowed_language = ['en', 'id'];

		if (in_array($this->input->get('id'), $allowed_language))
		{
			set_cookie('language', $this->input->get('id'), 3600*24*365, '', '/', 'ardev_breadtalk_', NULL, TRUE);
			redirect('', 1);
		}
		else
		{
			echo 'Your language is not allowed by system';
			exit;
		}
	}

	public function test($translate_from, $translate_to, $country_code)
	{
		if ($country_code == 'id')
		{
			return $translate_to;
		}
		elseif ($country_code == 'en')
		{
			return $translate_from;
		}
	}

	public function asd()
	{
		echo $this->test('Help', 'Bantuan', 'en');
		exit;
	}
}

?>