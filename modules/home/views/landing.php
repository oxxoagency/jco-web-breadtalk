<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	/*
	 * Update link category to direct product page
	 *
	 */

	section_content('
	<div class="bt-slider" uk-slider="autoplay: true;autoplay-interval: 3000;pause-on-hover: true">
		<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
			<ul class="uk-slider-items uk-child-width-1-1">');

		foreach ($banners as $key) 
		{
			section_content('
				<li>
					<img src="'.$key['banner_image_desktop'].'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.$key['banner_image_mobile'].'" class="w-100 img-fluid d-block d-md-none">
				</li>');
		}

	section_content('
				<!---
				<li>
					<img src="'.base_url('assets/image_desktop/banner_homepage/BT-DESKTOP-WEB-BANNER-1280x449px-COVER-CATALOG1_edited.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/banner_homepage/BT-MOBILE-WEB-BANNER-414x296px-COVER-CATALOG1.jpg').'" class="w-100 img-fluid d-block d-md-none">
				</li>

				<li>
					<img src="'.base_url('assets/image_desktop/banner_homepage/BT-DESKTOP-WEB-BANNER-1280x449px-COVER-CATALOG2_edited.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/banner_homepage/BT-MOBILE-WEB-BANNER-414x296px-COVER-CATALOG2.jpg').'" class="w-100 img-fluid d-block d-md-none">
				</li>

				<li>
					<img src="'.base_url('assets/image_desktop/banner_homepage/BT-DESKTOP-WEB-BANNER-1280x449px-COVER-CATALOG3_edited.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/banner_homepage/BT-MOBILE-WEB-BANNER-414x296px-COVER-CATALOG3.jpg').'" class="w-100 img-fluid d-block d-md-none">
				</li>
				--->
			</ul>
		</div>
		
		<ul class="uk-slider-nav uk-dotnav uk-flex-center"></ul>
	</div>
	
	<div class="container my-5">
		<div class="text-center mb-5">');

	if (getCountryUser() == 'id')
	{
		section_content('
			<div class="subheading-ob-lr mb-3">Tentang Kami</div>');
	}
	elseif (getCountryUser() == 'en') 
	{
		section_content('
			<div class="subheading-ob-lr mb-3">Our Product</div>');
	}

	section_content('
			<div class="h1 d-none d-md-block">'.$header['header_caption'].'</div>
			<div class="h3 d-block d-md-none">'.$header['header_caption'].'</div>
		</div>
	</div>

	<div class="mb-5 pb-5">
		<div class="row no-gutters">');

	foreach ($categories as $key) 
	{
		$initCategoryName = str_replace(" ", "_", $key['category_name']);
		$initCategoryName = str_replace("&", "", $initCategoryName);
		$initCategoryName = str_replace("!", "", $initCategoryName);
		$initCategoryName = str_replace("(", "", $initCategoryName);
		$initCategoryName = str_replace(")", "", $initCategoryName);

		$getCategoryName = str_replace("'", "", $initCategoryName);

		section_content('
			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.$key['category_image_desktop'].'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.$key['category_image_mobile'].'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">'.$key['category_name'].'</h2>
							<a href="'.site_url('product?category='.$key['category_id'].'&name='.$getCategoryName).'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>');
	}

	section_content('
			<!---
			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-COOKIES.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-COOKIES.jpg').'" class="w-100 img-fluid d-block d-md-none">
					
					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Cookies</h2>
							<a href="'.site_url('product?category=cookies').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-DANISH.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-DANISH.jpg').'" class="w-100 img-fluid d-block d-md-none">
					
					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Danish</h2>
							<a href="'.site_url('product?category=danish').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-DRY-CAKE.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-DRY-CAKE.jpg').'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Dry Cakes</h2>
							<a href="'.site_url('product?category=dry_cake').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-HAMPERS.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-HAMPERS.jpg').'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Hampers</h2>
							<a href="'.site_url('product?category=hampers').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-SLICE-CAKE.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-SLICE-CAKE.jpg').'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Slice Cakes</h2>
							<a href="'.site_url('product?category=slice_cake').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 mb-4">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-TOAST.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-TOAST.jpg').'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Toast</h2>
							<a href="'.site_url('product?category=toast').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="bt-image-w-overlay">
					<img src="'.base_url('assets/image_desktop/category_homepage/BT-DESKTOP-CATEGORY-1280x435px-WHOLE-CAKE.jpg').'" class="w-100 img-fluid d-none d-md-block">
					<img src="'.base_url('assets/image_mobile/category_homepage/BT-MOBILE-CATEGORY-366x272px-WHOLE-CAKE.jpg').'" class="w-100 img-fluid d-block d-md-none">

					<div class="caption p-4 p-md-5 d-flex align-items-end">
						<div>
							<h2 class="text-white font-weight-bold mb-2 mb-md-3">Whole Cake</h2>
							<a href="'.site_url('product?category=whole_cake').'" class="text-bt-orange font-weight-semi-bold">See More <i class="fas fa-chevron-right fa-fw"></i></a>
						</div>
					</div>
				</div>
			</div>
			--->
		</div>
	</div>');

?>