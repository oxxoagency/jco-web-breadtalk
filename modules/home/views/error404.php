<?php

	/*
	 *	Aruna Development Project
	 *	IS NOT FREE SOFTWARE
	 *	Codename: Aruna Personal Site
	 *	Source: Based on Sosiaku Social Networking Software
	 *	Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *	Created and developed by Andhika Adhitia N
	 */

	defined('MODULEPATH') OR exit('No direct script access allowed');

	section_content('
	<style>
	.bt-notfound-page::before
	{
		background-image: url('.base_url('assets/images/background_notfound.svg').');
	}

	.bt-bg-illustration::before
	{
		background-image: url('.base_url('assets/images/notfound_ghost.svg').');
	}

	@media (min-width: 768px)
	{
		.bt-parent-container 
		{
			margin-top: 3.6rem;
		}
	}

	@media (min-width: 992px)
	{
		.bt-parent-container 
		{
			margin-top: 4.3rem;
		}
	}
	</style>

	<div class="bt-notfound-page">
		<div class="container text-center h-100 py-5 d-flex justify-content-center" style="z-index: 1">
			<div>
				<h3 class="text-bt-orange font-weight-bold mb-3">404</h3>
				<h1 class="mb-5 font-weight-bold">Page Not Found</h1>
					
				<div class="bt-bg-illustration">
					<img src="'.base_url('assets/images/notfound_illustration_wo_ghost.svg').'" class="img-fluid bt-illustration-ghost" style="width: 400px">
				</div>
			</div>
		</div>
	</div>');

?>