// Sample data values for populate map
// Update 26052021

axios.get(site_url+'location/getDataMap')
.then(response => 
{
	const ResDataMAP = response.data;

	// console.log(ResDataMAP);

	let data = [
		{"loc": [-6.194622509876034, 106.8200904814158], "title": "BreadTalk Grand Indonesia"},
		{"loc": [-6.905361929888867, 107.59689136792738], "title": "BreadTalk [Istana Plaza]"},
		{"loc": [-6.892689710143398, 107.60550359491303], "title": "BreadTalk Ciwalk"}
	];

	// Set center from first location
	let map = new L.Map("map", {zoom: 5, center: new L.latLng(-2.548926, 118.0148634) });

	// Base layer
	map.addLayer(new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"));

	// Layer contain searched elements
	let markersLayer = new L.LayerGroup();

	map.addLayer(markersLayer);

	let controlSearch = new L.Control.Search({
		position: "topleft",		
		layer: markersLayer,
		initial: true,
		zoom: 14,
		marker: false,
		textPlaceholder: "Find Location"
	});

	map.addControl(controlSearch);
	map.zoomControl.setPosition("topright");

	let icon_marker = L.icon(
	{
		iconUrl:		base_url+"assets/images/icon-marker.svg",
		iconSize:		[24, 39], // Size of the icon
		iconAnchor:		[12, 35],
		popupAnchor:	[1, -40]
	});

	// Populate map with markers from sample data
	for (i in ResDataMAP) 
	{
		let title = ResDataMAP[i].title,	// Value searched
			loc = ResDataMAP[i].loc,		// Position found
			marker = new L.Marker(new L.latLng(loc), {title: title, icon: icon_marker}); // Set property searched
		marker.bindPopup("<div class=\"text-left font-weight-bold pt-2\">"+title+"</div>");
		markersLayer.addLayer(marker);

		console.log(ResDataMAP[i].loc);
	}

	document.getElementById("searchtext13").onchange = function(e) 
	{
		console.log(e.target.value);
	};

	document.getElementsByClassName("search-button")[0].onclick = function() 
	{
		console.log(document.getElementById("searchtext13").value);
	};
})
.catch(function(error) 
{
	console.log(error);
});