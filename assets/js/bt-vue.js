Vue.component('paginate', VuejsPaginate);

Vue.use(VueLazyload, 
{
	observer: true,
	error: 'assets/plugins/fontawesome/5.15.2/svgs/solid/image.svg',
	loading: 'assets/plugins/fontawesome/5.15.2/svgs/solid/redo-alt.svg'
});

const VueSearchProduct = new Vue(
{
	el: '#bt-search-product',
	components: 
	{
		VueLazyload
	},
	data: 
	{
		getData: {},
		getSort: '',
		getCat: '',
		getResponse: '',
		getTitleCategory: 'All',
		pageCount: '',
		pageRange: '',
		pageUrl: '',
		pageCategory: '',
		selectedCat: '',
		currPage: '',
		statusData: '',
		msgData: '',
		show: true,
		loading: true,
		loadingnextpage: true,
		responseMessage: ''
	},
	methods:
	{
		getNormalizeString: function(string)
		{
			let i, frags = string.split('_');
			
			for (i = 0; i < frags.length; i++) 
			{
				frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
			}
			
			return frags.join(' ');
		},
		getFetchData: function()
		{
			if (document.querySelector(".bt-data-fetch") !== null && 
				document.querySelector(".bt-data-fetch").getAttribute("data-url") !== null)
			{
				const url = document.querySelector(".bt-data-fetch").getAttribute("data-url");
				const url2 = document.location.toString();
				const newURL = new URL(url2);

				if (newURL.searchParams.get("name") !== null)
				{
					const initCategoryName = newURL.searchParams.get("name").charAt(0).toUpperCase()+newURL.searchParams.get("name").slice(1);

					this.selectedCat = initCategoryName.replace("_", "");
					this.getTitleCategory = initCategoryName.replace("_", " ");
				}
				
				if (url2.match(/(?:category=)+[a-z0-9]+/g)) 
				{
					this.getCat = '?category='+url2.split("=")[1];

					document.querySelector("#bt-item").scrollIntoView(true);
				}

				axios.get(url+this.getCat)
				.then(response => 
				{
					const getval = response.data.slice(-1)[0];
					const getRes = response.data.slice(0)[0];

					this.getData = response.data;
					// this.pageCount = Math.ceil(getval.getDataPage.total/16);
					this.pageCount = getval.getDataPage.total;
					this.pageRange = getval.getDataPage.num_per_page;
					this.currPage = getval.getDataPage.current_page;
					this.statusData = getRes.status;
					this.msgData = getRes.msg;
				})
				.catch(function(error) 
				{
					console.log(error);
				})
				.finally(() => 
				{ 
					this.loading = false;
					this.loadingnextpage = false;
				});
			}

			if (document.querySelector(".ar-data-status") !== null)
			{
				if (getComputedStyle(document.querySelector('.ar-data-status'), null).display == 'none')
				{
					document.querySelector(".ar-data-status").style.display = 'block';
				}
			}

			if (document.querySelector(".ar-data-load") !== null)
			{
				if (getComputedStyle(document.querySelector('.ar-data-load'), null).display == 'none')
				{
					document.querySelector(".ar-data-load").style.display = 'block';
				}
			}
		},
		clickCollapseToDown: function(key)
		{
			if (key.target.childNodes[0].classList.contains("bt-collapses-active"))
			{
				key.target.childNodes[0].classList.remove("bt-collapses-active");
			}
			else
			{
				key.target.childNodes[0].classList.add("bt-collapses-active");
			}			

			/*
			const getArrowCollapse = document.querySelectorAll(".bt-collapses");

			for (let i = 0; i < getArrowCollapse.length; i++) 
			{
				let initChildNodes = key.originalTarget.childNodes.length ? key.originalTarget.childNodes[i] : null;

				if (key.originalTarget.childNodes[i].classList.contains("bt-collapses-active"))
				{
					key.originalTarget.childNodes[i].classList.remove("bt-collapses-active");
				}
				else
				{
					key.originalTarget.childNodes[i].classList.add("bt-collapses-active");
				}

				console.log(key.originalTarget.childNodes[i]);
			}
			*/
		},
		sortBy: function(getSortBy)
		{
			if (document.querySelector(".bt-data-fetch") !== null && 
				document.querySelector(".bt-data-fetch").getAttribute("data-url") !== null)
			{
				const url = document.querySelector(".bt-data-fetch").getAttribute("data-url");
				const url2 = new URL(window.location.href);

				if (url2.searchParams.get('category'))
				{
					const urlSearchParams = new URLSearchParams(window.location.search);
					const params = Object.fromEntries(urlSearchParams.entries());

					this.pageCategory = params['category'];
				}
				
				this.getSort = getSortBy;

				if (this.pageCategory != '')
				{
					this.getCat = '?category='+this.pageCategory+'&sort='+this.getSort;
				}
				else
				{
					this.getCat = '?sort='+this.getSort;
				}

				document.querySelector("#bt-item").scrollIntoView(true);

				this.loading = true;

				axios.get(url+this.getCat)
				.then(response => 
				{
					const getval = response.data.slice(-1)[0];
					const getRes = response.data.slice(0)[0];

					this.getData = response.data
					this.pageCount = getval.getDataPage.total;
					this.pageRange = getval.getDataPage.num_per_page;
					this.currPage = getval.getDataPage.current_page;
					this.statusData = getRes.status;
					this.msgData = getRes.msg;
				})
				.catch(function(error) 
				{
					console.log(error);
				})
				.finally(() => this.loading = false);
			}
		},
		selectCategory: function(getCategory, getCategoryName) 
		{
			if (document.querySelector(".bt-data-fetch") !== null && 
				document.querySelector(".bt-data-fetch").getAttribute("data-url") !== null)
			{
				const url = document.querySelector(".bt-data-fetch").getAttribute("data-url");
				const url2 = new URL(window.location.href);

				this.pageCategory = getCategory;
				this.selectedCat = getCategoryName.replace(" ", "");
				this.getTitleCategory = getCategoryName.replace("_", " ");

				if (url2.searchParams.get('category'))
				{
					window.history.replaceState({}, document.title, "/" + "product?category="+getCategory+"&name="+this.selectedCat);
				}

				if (this.getSort != '')
				{
					this.getCat = '?category='+this.pageCategory+'&sort='+this.getSort;
				}
				else
				{
					this.getCat = '?category='+this.pageCategory;
				}

				document.querySelector("#bt-item").scrollIntoView(true);

				console.log(this.selectedCat);

				this.loading = true;

				axios.get(url+this.getCat)
				.then(response => 
				{
					const getval = response.data.slice(-1)[0];
					const getRes = response.data.slice(0)[0];

					this.getData = response.data
					this.pageCount = getval.getDataPage.total;
					this.pageRange = getval.getDataPage.num_per_page;
					this.currPage = getval.getDataPage.current_page;
					this.statusData = getRes.status;
					this.msgData = getRes.msg;
				})
				.catch(function(error) 
				{
					console.log(error);
				})
				.finally(() => this.loading = false);
			}
		},
		clickPaginate: async function(page) 
		{
			if (document.querySelector(".bt-data-fetch") !== null && 
				document.querySelector(".bt-data-fetch").getAttribute("data-url") !== null)
			{
				const url = document.querySelector(".bt-data-fetch").getAttribute("data-url");
				const url2 = new URL(window.location.href);

				if (url2.searchParams.get('category'))
				{
					const urlSearchParams = new URLSearchParams(window.location.search);
					const params = Object.fromEntries(urlSearchParams.entries());

					this.pageCategory = params['category'];
				}

				if (this.getSort != '')
				{
					if (page == 1)
					{
						if (this.pageCategory != '')
						{
							this.pageUrl = '?category='+this.pageCategory+'&sort='+this.getSort;
						}
						else
						{
							this.pageUrl = '?sort='+this.getSort;
						}
					}
					else
					{
						if (this.pageCategory != '')
						{
							this.pageUrl = '?category='+this.pageCategory+'&sort='+this.getSort+'&page='+page;
						}
						else
						{
							this.pageUrl = '?sort='+this.getSort+'&page='+page;
						}
					}
				}
				else if (this.pageCategory != '')
				{
					if (page == 1)
					{
						if (this.getSort != '')
						{
							this.pageUrl = '?category='+this.pageCategory+'&sort='+this.getSort;
						}
						else
						{
							this.pageUrl = '?category='+this.pageCategory;
						}
					}
					else
					{
						if (this.getSort != '')
						{
							this.pageUrl = '?category='+this.pageCategory+'&sort='+this.getSort+'&page='+page;
						}
						else
						{
							this.pageUrl = '?category='+this.pageCategory+'&page='+page;
						}
					}
				}
				else
				{
					if (page == 1)
					{
						this.pageUrl = '';
					}
					else
					{
						this.pageUrl = '?page='+page;
					}
				}

				this.loadingnextpage = true;
				document.querySelector("#bt-item").scrollIntoView(true);

				await axios.get(url+this.pageUrl)
				.then(response => 
				{
					const getval = response.data.slice(-1)[0];

					if (this.currPage >= this.pageCount)
					{
						this.currPage = '';
					}

					this.getData = response.data
					this.pageCount = getval.getDataPage.total;
					this.pageRange = getval.getDataPage.num_per_page;
				})
				.catch(function(error) 
				{
					console.log(error);
				})
				.finally(() => 
				{ 
					this.loading = false;
					this.loadingnextpage = false;
				});
			}
		}
	},
	created: function()
	{
		this.getFetchData();
	}
});

const Vue2Form = new Vue(
{
	el: "#ar-app-form",
	data: 
	{
		form: {},
		responseMessage: '',
		responseGRecaptcha: '',
		initResponse: [],
		initForm: 
		{
			'email': null,
			'fullname': null,
			'subject': null,
			'phone_number': null,
			'message': null
		}
	},
	methods: 
	{
		submitWithValidate: function(event)
		{
			event.preventDefault();

			// Get value of attribute in HTML.
			let formActionURL = document.getElementById("ar-app-form").getElementsByTagName("form")[0].getAttribute("action"); 
			let formMethod = document.getElementById("ar-app-form").getElementsByTagName("form")[0].getAttribute("method");
			let formReset = document.getElementById("ar-app-form").getElementsByTagName("form")[0].getAttribute("data-reset");
			let captchaReset = document.getElementById("ar-app-form").getElementsByTagName("form")[0].getAttribute("captcha-reset");

			// Get value of submit button.
			let getValueButton = document.querySelector('input[type="submit"]').getAttribute('value');

			// FormData objects are used to capture HTML form and submit it using fetch or another network method.
			let formData = new FormData(this.$refs.formHTML);

			// Get class button name to change the button to button loading state .
			document.getElementsByClassName("btn-malika")[0].insertAdjacentHTML("beforebegin", "<a class=\"btn btn-secondary btn-loading btn-lg font-size-inherit px-5 py-3\">Submitting <div class=\"spinner-border spinner-border-sm text-light ml-1\" role=\"status\"><span class=\"sr-only\">Loading...</span></div></a>");
			document.getElementsByClassName("btn-malika")[0].remove();

			axios(
			{
				url: formActionURL,
				method: formMethod,
				data: formData,
				headers: {"Content-Type": "multipart/form-data"}
			})
			.then(response => 
			{
				if (response.data.status == 'ok')
				{
					if ( ! response.data.url)
					{
						// Get data from response data
						this.responseMessage = response.data.msg;

						let toast = document.getElementsByClassName("sk-notice-toast")[0];
						toast.innerHTML = "<div class=\"bt-alert position-fixed m-3\"><div class=\"toast sk-toast-failed bg-success text-white\" data-autohide=\"false\"><div class=\"toast-body pt-3 px-3 pb-n3\"><h5 class=\"h6 mb-3\"><i class=\"fas fa-exclamation-triangle mr-2\"></i> Notice</h5> "+this.responseMessage+"</div></div></div>"; 

						// We still use JQuery because the Toast need JQuery to run
						$(".toast").toast("show");

						document.getElementsByClassName("btn-loading")[0].insertAdjacentHTML("beforebegin", "<input type=\"submit\" class=\"btn btn-bt-orange btn-malika btn-lg font-size-inherit px-5 py-3\" value=\""+getValueButton+"\">");
						document.getElementsByClassName("btn-loading")[0].remove();
					}
					else
					{
						window.setTimeout(function() 
						{
							window.location.href = response.data.url;
						}, 500);

						document.getElementsByClassName("btn-loading")[0].insertAdjacentHTML("beforebegin", "<a class=\"btn btn-success btn-logged btn-lg font-size-inherit px-5 py-3\">Success <i class=\"far fa-check-circle fa-fw mr-1\"></i></div></a>");
						document.getElementsByClassName("btn-loading")[0].remove();
					}

					if (formReset == "true")
					{
						grecaptcha.reset();
						
						document.getElementById("ar-app-form").getElementsByTagName("form")[0].reset();
					}

					// Set to false if all form have no errors
					this.initForm['email'] = false;
					this.initForm['fullname'] = false;
					this.initForm['subject'] = false;
					this.initForm['phone_number'] = false;
					this.initForm['message'] = false;

					// Set value of Google Recaptcha message to empty or null
					this.responseGRecaptcha = '';
				}
				else if (response.data.status == 'fail')
				{
					// Get data from response data
					this.responseMessage = response.data.msg;

					// Get data from Google Recaptcha
					this.responseGRecaptcha = this.responseMessage['g-recaptcha-response'];

					// Check variable if form have notice error and set to true
					this.initForm['email'] = (this.responseMessage['email'] != undefined) ? true : false;
					this.initForm['fullname'] = (this.responseMessage['fullname'] != undefined) ? true : false;
					this.initForm['subject'] = (this.responseMessage['subject'] != undefined) ? true : false;
					this.initForm['phone_number'] = (this.responseMessage['phone_number'] != undefined) ? true : false;
					this.initForm['message'] = (this.responseMessage['message'] != undefined) ? true : false;

					document.getElementsByClassName("btn-loading")[0].insertAdjacentHTML("beforebegin", "<input type=\"submit\" class=\"btn btn-bt-orange btn-malika btn-lg font-size-inherit px-5 py-3\" value=\""+getValueButton+"\">");
					document.getElementsByClassName("btn-loading")[0].remove();
				}
				else if (response.data.status == 'fail-submitted')
				{
					// Get data from response data
					this.responseMessage = response.data.msg;

					let toast = document.getElementsByClassName("sk-notice-toast")[0];
					toast.innerHTML = "<div class=\"bt-alert position-fixed m-3\"><div class=\"toast sk-toast-failed bg-danger text-white\" data-autohide=\"false\"><div class=\"toast-body pt-3 px-3 pb-n3\"><h5 class=\"h6 mb-3\"><i class=\"fas fa-exclamation-triangle mr-2\"></i> Notice</h5> "+this.responseMessage+"</div></div></div>"; 

					// We still use JQuery because the Toast need JQuery to run
					$(".toast").toast("show");

					if (captchaReset == "true")
					{
						grecaptcha.reset();
					}

					document.getElementsByClassName("btn-loading")[0].insertAdjacentHTML("beforebegin", "<input type=\"submit\" class=\"btn btn-bt-orange btn-malika btn-lg font-size-inherit px-5 py-3\" value=\""+getValueButton+"\">");
					document.getElementsByClassName("btn-loading")[0].remove();	
				}
			})
			.catch(response =>
			{
				console.log(response);
			});
		}
	}
});