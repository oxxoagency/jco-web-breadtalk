<?php

	/**
	 * Fix bug arrow navigation horizontal if iPad Pro view with 1024px
	 */

	defined('THEMEPATH') OR exit('No direct script access allowed');
	
	display_application_header('
	<!doctype html>
	<html lang="en">
		<head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<!-- Favicons -->
			<link rel="apple-touch-icon" href="'.base_url('assets/images/apple-icon-180x180.png').'" sizes="180x180">
			<link rel="icon" href="'.base_url('assets/images/apple-icon-72x72.png').'" sizes="72x72">
			<link rel="icon" href="'.base_url('assets/images/apple-icon-36x36.png').'" sizes="36x36">

			<!-- Bootstrap CSS -->
			<link rel="stylesheet" href="'.base_url('assets/plugins/bootstrap/4.6.0/css/bootstrap.min.css').'">

			<!-- Font Montserrat CSS -->
			<link rel="preconnect" href="https://fonts.gstatic.com">
			<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap"> 

			<!-- Custom CSS -->
			<link rel="stylesheet" href="'.base_url('assets/css/aruna-v2.css?v=0.0.2').'">
			<link rel="stylesheet" href="'.base_url('assets/plugins/fontawesome/5.15.2/css/all.min.css').'">
			<link rel="stylesheet" href="'.base_url('assets/plugins/uikit-compatible-w-bootstrap/3.6.14/css/uikit.css').'">
			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
			
			<title>BreadTalk</title>
		</head>

		<body>
			<div class="bt-overlay"></div>

			<header class="navbar navbar-expand-lg bt-navbar fixed-top py-lg-0">
				<div class="container-lg">
					<div class="navbar-for-mobile">
						<div>
							<a class="navbar-brand" href="'.site_url().'"><img src="'.base_url('assets/images/breadtalk_logo.svg').'"></a>
						</div>

						<div class="d-flex align-items-center">
							<button class="navbar-toggler navbar-toggler-icon-open border-0" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon" style="background-image: url('.base_url('assets/images/menu_open.svg').')"></span>
							</button>

							<button class="navbar-toggler navbar-toggler-icon-close border-0" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon" style="background-image: url('.base_url('assets/images/menu_close.svg').')"></span>
							</button>
						</div>
					</div>

					<div class="collapse navbar-collapse pl-lg-5" id="navbarMenu">					
						<ul class="navbar-nav mx-lg-auto mt-2 mt-lg-0 text-center">');

					if (getCountryUser() == 'id')
					{
						display_application_header('
							<li class="nav-item '.getNavMenu('home').'">
								<a class="nav-link" href="'.site_url().'">Beranda</a>
							</li>

							<li class="nav-item '.getNavMenu('promotion').'">
								<a class="nav-link" href="'.site_url('promotion').'">Promo</a>
							</li>

							<li class="nav-item '.getNavMenu('product').'">
								<a class="nav-link" href="'.site_url('product').'">Produk</a>
							</li>

							<li class="nav-item '.getNavMenu('location').'">
								<a class="nav-link" href="'.site_url('location').'">Lokasi</a>
							</li>

							<li class="nav-item '.getNavMenu('contactus').'">
								<a class="nav-link" href="'.site_url('contactus').'">Hubungi Kami</a>
							</li>');
					}
					elseif (getCountryUser() == 'en') 
					{
						display_application_header('
							<li class="nav-item '.getNavMenu('home').'">
								<a class="nav-link" href="'.site_url().'">Home</a>
							</li>

							<li class="nav-item '.getNavMenu('promotion').'">
								<a class="nav-link" href="'.site_url('promotion').'">Promo</a>
							</li>

							<li class="nav-item '.getNavMenu('product').'">
								<a class="nav-link" href="'.site_url('product').'">Product</a>
							</li>

							<li class="nav-item '.getNavMenu('location').'">
								<a class="nav-link" href="'.site_url('location').'">Location</a>
							</li>

							<li class="nav-item '.getNavMenu('contactus').'">
								<a class="nav-link" href="'.site_url('contactus').'">Contact Us</a>
							</li>');
					}

	display_application_header('
							<li class="nav-item dropdown d-block d-lg-none">
								<a class="nav-link dropdown-toggle" href="#!" id="navbarChangeLang" role="button" data-toggle="dropdown" aria-expanded="false">'.strtoupper(getCountryUser()).'</a>
							
								<ul class="dropdown-menu bg-white text-center" aria-labelledby="navbarChangeLang">
									<li><a class="dropdown-item border-0 rounded-0 py-3" href="'.site_url('home/setlanguage?id=en').'">English</a></li>
									<li><a class="dropdown-item border-0 rounded-0 py-3" href="'.site_url('home/setlanguage?id=id').'">Indonesian</a></li>
								</ul>
							</li>

							<li class="nav-item d-block d-lg-none">
								<a class="nav-link" href="https://order.btdelivery.com" target="_blank"><img src="'.base_url('assets/images/icon-cart.svg').'" class="img-fluid align-middle mr-1"> <span class="text-bt-white font-weight-semi-bold position-relative" style="top: .2em">ORDER DELIVERY</span></a>
							</li>
						</ul>

						<ul class="navbar-nav ml-lg-auto mt-lg-2 mt-lg-0 d-none d-lg-flex">
							<li class="nav-item">
								<a class="nav-link-alt" href="https://order.btdelivery.com" target="_blank"><img src="'.base_url('assets/images/icon-cart.svg').'" class="img-fluid align-middle mr-1"> <span class="text-bt-white font-weight-semi-bold position-relative d-none d-xl-inline-block" style="top: .2em">ORDER DELIVERY</span></a>
							</li>

							<li class="nav-item dropdown">
								<a class="nav-link-alt mx-0 text-bt-white font-weight-semi-bold dropdown-toggle" href="#!" id="navbarChangeLangWeb" role="button" data-toggle="dropdown" aria-expanded="false">'.strtoupper(getCountryUser()).'</a>
							
								<ul class="dropdown-menu dropdown-menu-right bg-white text-left font-size-inherit shadow mt-2" aria-labelledby="navbarChangeLangWeb">
									<li><a class="dropdown-item border-0 rounded-0 py-2" href="'.site_url('home/setlanguage?id=en').'">English</a></li>
									<li><a class="dropdown-item border-0 rounded-0 py-2" href="'.site_url('home/setlanguage?id=id').'">Indonesian</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</header>

			<div class="bt-parent-container">');

	// Load application from modules
	display_application_content();

	display_application_footer('
			</div>

			<footer class="bt-footer bg-bt-black-linear position-relative text-white p-4 p-lg-5">
				<div class="row no-gutters">
					<div class="col-md-6 mb-5 mb-md-0">
						<img src="'.base_url('assets/images/breadtalk_logo.svg').'" class="img-fluid mb-4">
						
						<p class="text-white">
							Copyright © 2020 PT. TALKINDO SELAKSA ANUGRAH. All Rights Reserved
						</p>

						<div class="mt-5 pt-5 d-none d-lg-block">
							<div class="d-inline-block">
								<a href="https://www.facebook.com/Breadtalkindo">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-facebook-f fa-lg fa-fw text-white position-relative" style="top: 18%;left: 11%"></i>
									</div>
								</a>
							</div>

							<div class="d-inline-block mx-3">
								<a href="https://www.instagram.com/breadtalkindo/">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-instagram fa-lg fa-fw text-white position-relative" style="top: 18%;left: 15%"></i>
									</div>
								</a>
							</div>

							<div class="d-inline-block">
								<a href="https://twitter.com/breadtalkIndo">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-twitter fa-lg fa-fw text-white position-relative" style="top: 18%;left: 15%"></i>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4 mb-4 mb-md-0">
								<h6 class="mb-2">Company</h6>

								<div class="list-group list-group-flush">
									<a href="'.site_url('product').'" class="list-group-item list-group-item-action bg-transparent border-0 text-bt-grey px-0">Our Product</a>
									<a href="'.site_url('location').'" class="list-group-item list-group-item-action bg-transparent border-0 text-bt-grey px-0">Our Store</a>
								</div>
							</div>

							<div class="col-md-4 mb-4 mb-md-0">
								<h6 class="mb-2">Support</h6>

								<div class="list-group list-group-flush">
									<a href="'.site_url('privacy').'" class="list-group-item list-group-item-action bg-transparent border-0 text-bt-grey px-0">Privacy Policy</a>
								</div>
							</div>

							<div class="col-md-4 mb-4 mb-md-0">
								<h6 class="mb-2">Contact Us</h6>

								<div class="list-group list-group-flush">
									<a href="#" class="list-group-item list-group-item-action bg-transparent border-0 text-bt-grey px-0">0815-8998-000</a>
								</div>
							</div>
						</div>

						<div class="mt-4 d-block d-lg-none">
							<div class="d-inline-block">
								<a href="https://www.facebook.com/Breadtalkindo">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-facebook-f fa-lg fa-fw text-white position-relative" style="top: 18%;left: 11%"></i>
									</div>
								</a>
							</div>

							<div class="d-inline-block mx-3">
								<a href="https://www.instagram.com/breadtalkindo/">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-instagram fa-lg fa-fw text-white position-relative" style="top: 18%;left: 15%"></i>
									</div>
								</a>
							</div>

							<div class="d-inline-block">
								<a href="https://twitter.com/breadtalkIndo">
									<div class="rounded-circle bg-dark" style="width: 32px;height: 32px;">
										<i class="fab fa-twitter fa-lg fa-fw text-white position-relative" style="top: 18%;left: 15%"></i>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</footer>

			<div id="bt-btn-totop"></div>

			<!-- Optional JavaScript -->
			<!-- jQuery first, then Popper.js, then Bootstrap JS, and other -->
			<script>let base_url = "'.base_url().'"; let site_url = "'.site_url().'";</script>

			'.load_vuejs().'

			<script src="'.base_url('assets/js/jquery-3.5.1.min.js').'"></script>
			<script src="'.base_url('assets/plugins/bootstrap/4.6.0/js/bootstrap.bundle.min.js').'"></script>
			<script src="'.base_url('assets/plugins/fontawesome/5.15.2/js/all.min.js').'"></script>
			<script src="'.base_url('assets/plugins/uikit-compatible-w-bootstrap/3.6.14/js/uikit.min.js').'"></script>
			<script src="'.base_url('assets/plugins/uikit-compatible-w-bootstrap/3.6.14/js/uikit-icons.min.js').'"></script>

			<script>
			document.getElementsByClassName("navbar-toggler-icon-open")[0].onclick = function(e) 
			{
				e.preventDefault();

				document.getElementsByClassName("navbar-toggler-icon-open")[0].style.display = "none";
				document.getElementsByClassName("navbar-toggler-icon-close")[0].style.display = "block";
				document.getElementsByClassName("bt-overlay")[0].style.zIndex = "100";
				document.getElementsByClassName("bt-overlay")[0].style.display = "block";
				document.body.style.overflowY = "hidden";

				return false;
			}

			document.getElementsByClassName("navbar-toggler-icon-close")[0].onclick = function(e) 
			{
				e.preventDefault();

				document.getElementsByClassName("navbar-toggler-icon-open")[0].removeAttribute("style");
				document.getElementsByClassName("navbar-toggler-icon-close")[0].style.display = "none";
				document.getElementsByClassName("bt-overlay")[0].removeAttribute("style");
				document.body.removeAttribute("style");

				return false;
			}

			$(document).ready(function()
			{
				let scrollDuration = 500;
				let leftArrowNav = document.getElementsByClassName("bt-leftArrowNav")[0];
				let rightArrowNav = document.getElementsByClassName("bt-rightArrowNav")[0];

				let itemsLength = $(".bt-nav-category").outerWidth() == 1024 ? 7 : $(".bt-nav-list").length;
				let itemSize = $(".bt-nav-list").outerWidth(true);

				let paddleMargin = 20;

				let getMenuWrapperSize = function() 
				{
					return $(".bt-nav-category").outerWidth();
				}

				let menuWrapperSize = getMenuWrapperSize();

				$(window).on("resize", function() 
				{
					menuWrapperSize = getMenuWrapperSize();
				});

				let menuVisibleSize = menuWrapperSize;

				let getMenuSize = function() 
				{
					return itemsLength * itemSize;
				};

				let menuSize = getMenuSize();
				let menuInvisibleSize = menuSize - menuWrapperSize;

				let getMenuPosition = function() 
				{
					return $(".bt-nav-category").scrollLeft();
				};

				$(".bt-nav-category").on("scroll", function() 
				{
					menuInvisibleSize = menuSize - menuWrapperSize;
					let menuPosition = getMenuPosition();

					let menuEndOffset = menuInvisibleSize - paddleMargin;

					if (menuPosition <= paddleMargin) 
					{
						$(leftArrowNav).addClass("d-none");
						$(leftArrowNav).removeClass("d-inline-block");
						$(rightArrowNav).removeClass("d-none");
					} 
					else if (menuPosition < menuEndOffset) 
					{
						$(leftArrowNav).addClass("d-inline-block");
						$(leftArrowNav).removeClass("d-none");
						$(rightArrowNav).removeClass("d-inline-block");
						$(rightArrowNav).removeClass("d-none");
					} 
					else if (menuPosition >= menuEndOffset) 
					{
						$(leftArrowNav).removeClass("d-none");
						$(rightArrowNav).addClass("d-none");
						$(rightArrowNav).removeClass("d-inline-block");
					}
				});

				$(rightArrowNav).on("click", function() 
				{
					$(".bt-nav-category").animate( { scrollLeft: menuInvisibleSize}, scrollDuration);
				});

				$(leftArrowNav).on("click", function() 
				{
					$(".bt-nav-category").animate( { scrollLeft: "0" }, scrollDuration);
				});

				counter = function() 
				{
					const value = $(".text-totalchars").val();

					if (value.length == 0) 
					{
						$("#totalChars").html(0);
						return;
					}

					const totalChars = value.length;

					$("#totalChars").html(totalChars);
				};

				$(".text-totalchars").change(counter);
				$(".text-totalchars").keydown(counter);
				$(".text-totalchars").keypress(counter);
				$(".text-totalchars").keyup(counter);
				$(".text-totalchars").blur(counter);
				$(".text-totalchars").focus(counter);

				let btn = $("#bt-btn-totop");

				if (window.pageYOffset != 0)
				{
					btn.addClass("show");
				}

				$(window).scroll(function() 
				{
					if ($(window).scrollTop() > 300) 
					{
						btn.addClass("show");
					} 
					else 
					{
						btn.removeClass("show");
					}
				});

				btn.on("click", function(e) 
				{
					e.preventDefault();
					$("html, body").animate({scrollTop:0}, "300");
				});
			});
			</script>

			'.load_js().'
		</body>
	</html>');

?>