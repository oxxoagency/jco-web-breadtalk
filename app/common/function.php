<?php

	/*
	 *  Aruna Development Project
	 *  IS NOT FREE SOFTWARE
	 *  Codename: Ardev Cassandra
	 *  Source: Based on Sosiaku Social Networking Software
	 *  Website: https://www.sosiaku.gq
	 *	Website: https://www.aruna-dev.id
	 *  Created and developed by Andhika Adhitia N
	 */

	defined('APPPATH') OR exit('No direct script access allowed');

	// ------------------------------------------------------------------------

	/**
	 * Get Date
	 * 
	 * Berfungsi untuk mengubah waktu sistem UNIX dan menampilkan waktu umum
	 * 
	 * @return string
	 */

	function get_date($timeo, $type = 'time') 
	{
		// Default set timezone is +7 for Jakarta, Indonesia
		$timezone = +7;

		// Default set for some settings
		$settings = [
			'time_format' 	 => 'g:i a',
			'date_format' 	 => 'M jS Y',
			'date_today' 	 => 'Today',
			'date_yesterday' => 'Yesterday'
		];

		$timeline = $timeo+$timezone*3600;
		$current = time()+$timezone*3600;
		$it_s = intval($current - $timeline);
		$it_m = intval($it_s/60);
		$it_h = intval($it_m/60);
		$it_d = intval($it_h/24);
		$it_y = intval($it_d/365);

		$timec = time()-$timeo;

		if ($timec < 3600 && $timec >= 0) 
		{
			return ceil($timec/60).' menit lalu';
		}
		elseif ($timec < 12*3600 && $timec >= 0) 
		{
			return ceil($timec/3600).' jam lalu';
		}
		else 
		{
			if ($type == 'time') 
			{
				return gmdate($settings['date_format'].', '.$settings['time_format'], $timeline);
			}
			else 
			{
				return gmdate($settings['date_format'], $timeline);
			}
		}

		if ($type == 'date') 
		{
			return gmdate($settings['date_format'], $timeline);
		}
		else 
		{
			if (gmdate("j", $timeline) == gmdate("j", $current)) 
			{
				return $settings['date_today'].', '.gmdate($settings['time_format'], $timeline);
			}
			elseif (gmdate("j", $timeline) == gmdate("j", ($current-3600*24))) 
			{
				return $settings['date_yesterday'].', '.gmdate($settings['time_format'], $timeline);
			}
			return gmdate($settings['date_format'].', '.$settings['time_format'], $timeline);
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Format Size
	 * 
	 * Berfungsi untuk menampilkan ukuran berkas
	 * dalam satuan KB, MB, GB, TB, dsb.
	 * 
	 * @return string
	 */

	function format_size($file) 
	{
		if ( ! file_exists($file)) 
		{
			$bytes = '';
		}
		else {
			$bytes = filesize($file);
		}

		if ($bytes < 1024) 
		{
			return $bytes.' B';
		} 
		elseif ($bytes < 1048576) 
		{
			return round($bytes / 1024, 2).' KB';
		}
		elseif ($bytes < 1073741824) 
		{
			return round($bytes / 1048576, 2).' MB';
		}
		elseif ($bytes < 1099511627776) 
		{
			return round($bytes / 1073741824, 2).' GB';
		}
		elseif ($bytes < 1125899906842624) 
		{
			return round($bytes / 1099511627776, 2).' TB';
		}
		elseif ($bytes < 1152921504606846976) 
		{
			return round($bytes / 1125899906842624, 2).' PB';
		}
		elseif ($bytes < 1180591620717411303424) 
		{
			return round($bytes / 1152921504606846976, 2).' EB';
		}
		elseif ($bytes < 1208925819614629174706176) 
		{
			return round($bytes / 1180591620717411303424, 2).' ZB';
		}
		else 
		{
			return round($bytes / 1208925819614629174706176, 2).' YB';
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Register JS
	 * 
	 * Berfungsi untuk mendaftarkan file javascript per module
	 * 
	 * @return string
	 */

	function register_js($file = array())
	{
		$GLOBALS['register_js'] = implode("\r\n 			", $file);

		return $GLOBALS['register_js'];
	}

	// ------------------------------------------------------------------------

	/**
	 * Load JS
	 * 
	 * Berfungsi untuk menampilkan berkas javascript yang telah didaftarkan
	 * fungsi diletakan difolder tema
	 * 
	 * @return string
	 */

	function load_js()
	{
		return get_data_global('register_js');	
	}

	// ------------------------------------------------------------------------

	/**
	 * Register VueJS
	 * 
	 * Berfungsi untuk mendaftarkan file Vuejs per module
	 * 
	 * @return string
	 */

	function register_vuejs($file = array())
	{
		$GLOBALS['register_vuejs'] = implode("\r\n 			", $file);

		return $GLOBALS['register_vuejs'];
	}

	// ------------------------------------------------------------------------

	/**
	 * Load VueJS
	 * 
	 * Berfungsi untuk menampilkan berkas VueJS yang telah didaftarkan
	 * fungsi diletakan difolder tema
	 * 
	 * @return string
	 */

	function load_vuejs()
	{
		return get_data_global('register_vuejs');	
	}

	// ------------------------------------------------------------------------

	/**
	 * getNavMenu
	 * 
	 * Berfungsi untuk mendeteksi halaman, jika dihalaman yang lagi dibuka
	 * fungsi class pada menu navigasi akan aktif
	 * 
	 * @return string
	 */

	function getNavMenu(string $currect_page = '')
	{
		$ext = load_ext(['url']);

		return $currect_page === uri_string() ? 'active' : '';
	}

	// ------------------------------------------------------------------------
	
	function getCountryUser()
	{
		$ext = load_ext(['cookie']);

		$ip = getenv('HTTP_CLIENT_IP')?:
			  getenv('HTTP_X_FORWARDED_FOR')?:
			  getenv('HTTP_X_FORWARDED')?:
			  getenv('HTTP_FORWARDED_FOR')?:
			  getenv('HTTP_FORWARDED')?:
			  getenv('REMOTE_ADDR');

		if ($ip == '127.0.0.1')
		{
			$getCountryUser = ! get_cookie('ardev_breadtalk_language') ? 'id' : get_cookie('ardev_breadtalk_language');
		}
		else
		{
			$convertIPToArray = explode(", ", $ip); 	
			$res = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$convertIPToArray[0]));

			$detectCountryUser = $res['geoplugin_countryCode'] == 'ID' ? 'id' : 'en';
			$getCountryUser = ! get_cookie('ardev_breadtalk_language') ? ($detectCountryUser) : get_cookie('ardev_breadtalk_language');
		}

		return $getCountryUser;
	}

	// ------------------------------------------------------------------------
	
	function autoSetLanguage()
	{
		$ip = getenv('HTTP_CLIENT_IP')?:
			  getenv('HTTP_X_FORWARDED_FOR')?:
			  getenv('HTTP_X_FORWARDED')?:
			  getenv('HTTP_FORWARDED_FOR')?:
			  getenv('HTTP_FORWARDED')?:
			  getenv('REMOTE_ADDR');

		$input 	= load_lib('input');
		$ext 	= load_ext('cookie');
		$convertIPToArray = explode(", ", $ip);
		$getCountryCode = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$convertIPToArray[0]));

		if ($getCountryCode['geoplugin_countryCode'] == 'ID')
		{
			set_cookie('language', 'id', 3600*24*365, '', '/', 'ardev_breadtalk_', NULL, TRUE);
		}
		else
		{
			set_cookie('language', 'en', 3600*24*365, '', '/', 'ardev_breadtalk_', NULL, TRUE);
		}
	}

	// ------------------------------------------------------------------------

	function get_translate($translate_from, $translate_to, $country_code)
	{
		if ($country_code == 'id')
		{
			return $translate_to;
		}
		elseif ($country_code == 'en')
		{
			return $translate_from;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Get Language
	 * 
	 * Berfungsi untuk mendapatkan konfigurasi bahasa untuk pengguna
	 * 
	 * @return string
	 */

	function get_language()
	{
		$ext = load_ext(['cookie']);

		if (get_cookie('ardev_breadtalk_language'))
		{
			$lang = get_cookie('ardev_breadtalk_language');
		}
		else 
		{
			$lang = 'id';
		}

		return $lang;
	}

	// ------------------------------------------------------------------------

	/**
	 * Init Language
	 * 
	 * 
	 * @return string
	 */

	function unselected_language()
	{
		$ext = load_ext(['cookie']);
		$allowed_language = ['en' => 'english', 'id' => 'Indonesian'];

		if ( ! get_cookie('ardev_breadtalk_language'))
		{
			$lang = 'id';
		}
		else 
		{
			$lang = get_cookie('ardev_breadtalk_language');
		}

		foreach ($allowed_language as $key => $value) 
		{
			if ($key != $lang)
			{
				return '<li><a class="dropdown-item border-0 rounded-0 py-2" href="'.site_url('home/setlanguage?id='.$key).'">'.ucfirst($value).'</a></li>';
			}
		}
	}

	// ------------------------------------------------------------------------

	function error_page($message = '', $class = '')
	{
		if (empty($message))
		{
			$message = '<h4 class="h5 font-weight-normal" style="line-height: 1.6">Sorry sweetheart, I can\'t find the page you requested <i class="far fa-frown ml-1 fa-lg"></i></h6><div class="mt-3"><a href="javascript:history.back();" class="text-white"><i class="fas fa-long-arrow-alt-left mr-2"></i> Back to Previous Page</a></div>';
		}

		section_content('<div class="bg-danger text-white text-center rounded p-4 '.$class.'">'.$message.'</div>');
		stop_here();
	}

	// ------------------------------------------------------------------------

	if ( ! function_exists('cs_offset'))
	{
		function cs_offset()
		{
			return cs_num_per_page()*(get_data_global('page')-1);
		}
	}

	// ------------------------------------------------------------------------

	if ( ! function_exists('cs_num_per_page'))
	{
		function cs_num_per_page()
		{
			$cs_config = load_cs_config('config');

			return $cs_config->item('num_per_page_exam');
		}
	}

?>